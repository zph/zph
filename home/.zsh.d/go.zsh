export GOROOT=/usr/local/go
export GOPATH=/usr/local/go
export GOBIN=$GOROOT/bin/
export PATH=$PATH:$GOBIN
